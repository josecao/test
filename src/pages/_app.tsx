import React, { FC } from 'react'
import { AppProps } from 'next/app'

import 'antd/lib/style/themes/default.less'

/**
 * withRedux HOC
 * NextJS wrapper for Redux
 */

const CustomApp: FC<AppProps> = ({ Component, pageProps }) => (
  <Component {...pageProps} />
)

export default CustomApp