const withAntdLess = require('next-plugin-antd-less')

const dev = process.env.NODE_ENV !== 'production'

module.exports = withAntdLess({
  modifyVars: { '@primary-color': 'red' }, // optional
  lessVarsFilePath: './src/styles/variables.less', // optional 
  lessVarsFilePathAppendToEndOfContent: false, // optional
  cssLoaderOptions: {
    // ... 
    mode: "local",
    localIdentName: dev ? "[local]--[hash:base64:4]" : "[hash:base64:8]", // invalid! for Unify getLocalIdent (Next.js / CRA), Cannot set it, but you can rewritten getLocalIdentFn
    exportLocalsConvention: "camelCase",
    exportOnlyLocals: false,
    // ...
    getLocalIdent: (context, localIdentName, localName, options) => {
      return "whatever_random_class_name";
    },
  },

  distDir: 'build',
});